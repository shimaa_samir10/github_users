(function () {
    'use strict';

    angular.module('application', [
        'ui.router',
        'ngAnimate',

        //foundation
        'foundation',
        'foundation.dynamicRouting',
        'foundation.dynamicRouting.animations'
    ])
        .controller('UsersCtrl', ["$scope", "$state", "$http", function ($scope, $state, $http) {
            // Grab URL parameters - this is unique to FFA, not standard for
            // AngularJS. Ensure $state is included in your dependencies list
            // in the controller definition above.
            $scope.id = ($state.params.id || 1);

            if ($scope.id != '') {
                // We've got a URL parameter, so let's get the single entity's
                // data from our data source
                $http.get("https://api.github.com/users/" + $scope.id,
                    {cache: true})
                    .success(function (data) {
                        // If the request succeeds, assign our data to the 'user'
                        // variable, passed to our page through $scope
                        $scope['user'] = data;
                    });
                    // .error(function () {});
                $http.get("https://api.github.com/users", {cache: true})
                    .success(function (data) {
                        $scope['users'] = data;
                    });
            } else {
                // There is no ID, so we'll show a list of all Users.
                $http.get("https://api.github.com/users", {cache: true})
                    .success(function (data) {
                        $scope['users'] = data;
                    });
            }
            return $scope;

        }])
        .config(config)
        .run(run)
    ;

    config.$inject = ['$urlRouterProvider', '$locationProvider'];

    function config($urlProvider, $locationProvider) {
        $urlProvider.otherwise('/');

        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });

        $locationProvider.hashPrefix('!');
    }

    function run() {
        FastClick.attach(document.body);

    }
})();
